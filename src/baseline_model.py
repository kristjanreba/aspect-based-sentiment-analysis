import pandas as pd
from process_data import load_data

'''
1. Classify the whole text vector to a single sentiment (neutral is the most common)
2. Assign the predicted sentiment to all entities in the text

Model predicts a list of tuples from a single text file.
Each tuple contains an entity and its predicted sentiment.
Eg.: [(entity1, sent1), (entity2, sent2), (entity3, sent3)]

To evaluate the model we must beforehand construct a list of tuples
with annotated entities and their sentiment.

To do:

Funkcije za v datoteko process_data.py:
- napisi funkcijo preprocess_data(df)
- napisi funkcijo vectorize(df) (odvisno za kater model se odlocimo)
- sentiment v obliki stringa zakodiramo v stevilko (ni nujno)
Zelo negativno = 0
Negativno = 1
Nevtralno = 2
Pozitivno = 3
Zelo pozitivno = 4


Open questions:

- how to create embedding of a text that contains all the relevent information?
- how can we predict sentiment of an entity without text embedding?
- convert word formations as 'ni dober' to 'slab'
'''

def accuracy_single_text(true, pred):
    '''
    Returs a float number representing the accuracy of the model on a single text.

    Arg:
    true -- list of correct (entity, sentiment)
    pred -- dict of predicted (entity, sentiment)
    '''
    n_correct = 0
    for i in range(len(true)):
        if true[i][1] == pred[i][1]: n_correct += 1
    return n_correct / len(true)


def accuracy(true_all, pred_all):
    '''
    Returs a float number representing the accuracy of the model on multiple texts.

    Arg:
    true_all -- List of lists of tuples. For each text there is a list of tuples with correct (entity, sentiment).
    true_all -- List of lists of tuples. For each text there is a list of tuples with predicted (entity, sentiment).
    '''
    n_correct = 0
    n_all = 0
    for i in range(len(true_all)):
        true = true_all[i]
        pred = pred_all[i]
        for j in range(len(true)):
            if true[j][1] == pred[j][1]: n_correct += 1
        n_all += len(true)
    return n_correct / n_all


def baseline_accuracy(true_all):
    '''
    Returns a float that represents the accuracy of a model that always predicts "Nevtralno" sentiment.

    Arg:
    true_all -- List of lists of tuples. For each text there is a list of tuples with (entity, sentiment).
    '''
    n_correct = 0
    n_all = 0
    for l in true_all: # for each list in a list
        for t in l: # for each tuple in a list
            if t[1] == 'Nevtralno': n_correct += 1
        n_all += len(l)
    return n_correct / n_all


if __name__ == '__main__':
    # test accuracy_single_text
    true = [('e1','Nevtralno'),('e2','Negativno'),('e3','Pozitivno'),('e4','Nevtralno')]
    pred = [('e1','Pozitivno'),('e2','Negativno'),('e3','Pozitivno'),('e4','Nevtralno')]
    print(accuracy_single_text(true, pred) == 0.75)

    # test accuracy
    true_all = [[('e1','Nevtralno'),('e2','Negativno')],[('e3','Pozitivno'),('e4','Nevtralno')]]
    pred_all = [[('e1','Pozitivno'),('e2','Negativno')],[('e3','Pozitivno'),('e4','Nevtralno')]]
    print(accuracy(true_all, pred_all) == 0.75)

    # test baseline_accuracy
    true_all = [[('e1','Nevtralno'),('e2','Negativno')],[('e3','Pozitivno'),('e4','Nevtralno')]]
    print(baseline_accuracy(true_all) == 0.5)
