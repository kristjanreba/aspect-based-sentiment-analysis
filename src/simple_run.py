from utils import *
from process_data import load_data_2

from pathlib import Path

# file paths
raw_data_path = Path("..\\data\\raw\\")
processed_data_path = Path("..\\data\\processed\\")

# chose models from selection of:
# NB, MNB, LR, LightGBM, SVM, DNN, RandomForest
model = MNB
model_n = "MNB"


# define paramters for context extraction
CONTEXT_SIZE = 5 # local context size n-word left n-word right
ADVANCED_CONTEXT = False # choose between naive and advanced local context extraction
DEP_PARSE = True # dependeny parsing context extraction module
WHOLE_SENTENCE = True # for sentences with one entity take whole sentence as context


# load prelemmatized, POS tagged and dependeny parsed data
print("Loading and processing the data")
X, y = load_data_2(processed_data_path, DEP_PARSE, ADVANCED_CONTEXT,
                   WHOLE_SENTENCE, context_size=CONTEXT_SIZE)

# process data in appropirate form for ML algorithms
print("Vectorizing the data")
X_n, _ = n_gram(X, X, (1, 2))
y_n = y[:, 0].astype(int)

# 10-fold cross validation
print("Cross validation")
print("\nEvaluation for model: {}".format(model_n))
f1_score, std_dev = cross_valid(X_n, y_n, model)