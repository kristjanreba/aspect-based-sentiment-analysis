import pandas as pd
from process_data import load_data


class EntitySentimentAnalyzer():
    '''
    Steps for entity level sentiment classification
    1. preprocess data (stop words removal, stemming)
    2. feature vectorization (TF-IDF, Doc2vec)
    3. classify (RF, MLP)

    Model predicts a list of tuples from a single text file.
    Each tuple contains an entity and its predicted sentiment.
    Eg.: [(entity1, sent1), (entity2, sent2), (entity3, sent3)]
    '''

    def __init__(self):
        pass


    def train(self, data):
        '''
        Train classifer on the given dataset
        '''
        pass


    def predict(self, data):
        '''
        Make predictions on the given dataset.
        Return list of tuples. Each tuple contains an entity and its predicted sentiment.
        '''
        pass


if __name__ == '__main__':
    pass