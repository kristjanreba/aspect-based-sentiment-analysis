from utils import *
from process_data import *

from pathlib import Path
import pandas as pd


raw_data_path = Path("..\\data\\raw\\")
processed_data_path = Path("..\\data\\processed\\")

models = [NB, MNB, LR]
models_n = ["NB", "MNB", "LR"]

model_f1_scores_dict = {}
model_f1_std_dict = {}


MAX_CONTEXT_SIZE = 5
DEP_PARSE = [True, False]
ADVANCED_CONTEXT = [True, False]
WHOLE_SENTENCE = [True, False]

results = pd.DataFrame(columns=["model_name",
                                "context_size",
                                "adv_context",
                                "dep_parse",
                                "whole_sentence",
                                "f1_score",
                                "std_dev"])

for context_size in range(1, MAX_CONTEXT_SIZE + 1):
    for adv_context in ADVANCED_CONTEXT:
        for dep_pars in DEP_PARSE:
            for whole_sent in WHOLE_SENTENCE:

                X, y = load_data_2(processed_data_path, dep_pars, adv_context,
                                   whole_sent, context_size=context_size)

                X_n, _ = n_gram(X, X, (1, 2))
                y_n = y[:, 0].astype(int)

                for model, name in zip(models, models_n):
                    print("\nEvaluation for model: {}".format(name))
                    f1_score, std_dev = cross_valid(X_n, y_n, model)

                    curr_results = {"model_name": name,
                                    "context_size": context_size,
                                    "adv_context": adv_context,
                                    "dep_parse": dep_pars,
                                    "whole_sentence": whole_sent,
                                    "f1_score": f1_score,
                                    "std_dev": std_dev}
                    print(curr_results)
                    results = results.append(curr_results, ignore_index=True)


results.to_csv("results_weighted_f1_score.csv", index=False)


#                     if name not in model_f1_scores_dict.keys():
#                         model_f1_scores_dict[name] = [f1_score]
#                     else:
#                         model_f1_scores_dict[name].append(f1_score)


# print(model_f1_scores_dict)
# print(model_f1_scores_dict.values())
