from pathlib import Path
import pandas as pd
import numpy as np
import string
import stanza

import gensim
import nltk
from nltk.tokenize import word_tokenize 
from nltk.corpus import stopwords

import lemmagen.lemmatizer
from lemmagen.lemmatizer import Lemmatizer

from sklearn.feature_extraction.text import TfidfVectorizer

from src.context_extraction import document_context_extraction, document_context_based_on_dependencies


def text_from_tsv(f):
    """
    Returns only the pure text of the news and
    DataFrame of anotations of the correspoding text.
    """

    def parse_sentiment(x):
        if "Nevtralno" in x:
            return 0
        # TODO: Zelo pozitivno
        elif "Pozitivno" in x or "pozitivno" in x:
            return 1

        # TODO: Zelo negativno
        elif "Negativno" in x or "negativno" in x:
            return 2
        else:
            return x

    file_lines = f.readlines()

    # text always starts on the idx 6 of the file
    text = file_lines[6]

    # crop the #TEXT= and \n at the end
    text_c = text[6:-1]

    lines_stripped = [line.strip("\n").strip("\t") for line in file_lines[7:]]
    lines_splitted = [line.split("\t") for line in lines_stripped]

    columns = {0: "idx",
               1: "chars_idx",
               2: "token",
               3: "entity_type",
               4: "sentiment",
               5: "idk_1",
               6: "idk_2"
               }

    df = pd.DataFrame(lines_splitted)
    df = df.rename(columns=columns)
    df["orig_sentiment"] = df["sentiment"]
    df["sentiment"] = df["sentiment"].apply(parse_sentiment)

    # extract only idx (1-4 -> 4)
    # -1 for zero indexing...
    df["idx"] = df["idx"].apply(lambda x: int(x.split("-")[1]) - 1) 

    return text_c, df


def preprocess_data_3(tokens):
    '''
    Perform only and lemmatization.
    '''

    tokens = [token.lower() for token in tokens if token not in string.punctuation]

    # MASK is lematized to maska, so rather us maska
    stop_words = stopwords.words("slovene")
    lemmatizer = Lemmatizer(dictionary=lemmagen.DICTIONARY_SLOVENE)
    # perform lemmatization and stop word removal
    tokens = [lemmatizer.lemmatize(token) for token in tokens]

    return tokens


def preprocess_data_2(tokens):
    '''
    Removes string punctuations and stop words from `tokens`.
    '''
    tokens = [token.lower() for token in tokens if token not in string.punctuation]
    stop_words = stopwords.words("slovene")
    tokens = [token for token in tokens if token not in stop_words]

    return tokens


def preprocess_data(text):
    '''
    Perform tokenization, stop words removal and lemmatization.
    '''

    table = text.maketrans({key: None for key in string.punctuation})
    text = text.lower().translate(table)
    tokens = word_tokenize(text)

    stop_words = stopwords.words("slovene")
    lemmatizer = Lemmatizer(dictionary=lemmagen.DICTIONARY_SLOVENE)
    # perform lemmatization and stop word removal
    tokens = [lemmatizer.lemmatize(token) for token in tokens if token not in stop_words]

    return tokens


def vectorize(text_list):
    '''
    Perform TF-IDF, Doc2vec
    # TODO: Doc2vec

    Args:
        text_list -> list of strings(news text)
    '''
    tf_idf = TfidfVectorizer(tokenizer=preprocess_data,
                             stop_words=None)

    tfs = tf_idf.fit_transform(text_list)                  

    return tfs


def doc2vec(X_train, X_test):
    """
    Train embedding on X_train and then
    embedd X_train and X_test.

    X_train, X_test has to be list of tokens lists.

    TODO: Parameters of doc2vec embedding

    """

    def read_corpus(data, tokens_only=False):
        for idx, tokens in enumerate(data):
            if tokens_only:
                yield tokens
            else:
                # For training data, add tags
                yield gensim.models.doc2vec.TaggedDocument(tokens, [idx])

    train_corpus = list(read_corpus(X_train))
    test_corpus = list(read_corpus(X_test, tokens_only=True))

    # initi model
    model = gensim.models.doc2vec.Doc2Vec(vector_size=50, 
                                          min_count=2, 
                                          epochs=40)
    model.build_vocab(train_corpus)

    # train model
    model.train(train_corpus, total_examples=model.corpus_count, epochs=model.epochs)

    # generate train embedding
    X_train_em = []
    for doc_id in range(len(train_corpus)):
        embbeding = model.infer_vector(train_corpus[doc_id].words)
        X_train_em.append(embbeding)

    # generate test embedding
    X_test_em = []
    for doc_id in range(len(test_corpus)):
        embbeding = model.infer_vector(test_corpus[doc_id])
        X_test_em.append(embbeding)

    return np.array(X_train_em), np.array(X_test_em)

############# LASER #######################

def list_of_words_to_string(word_list):
    return ' '.join(word for word in word_list)


def remove_duplicates(l):
    '''
    Remove duplicates from a list of strings.
    '''
    return list(dict.fromkeys(l))


def embed_with_LSTM(X):
    """
    X is a list of lists of tokens.
    Create embedding for one entity from multiple sentences.
    """
    pass

############################################


def detect_negation(tokens, positive_words): 
    negation_words = ["ni", "ne", "nebo"]

    negation_idxs = []
    pos_words_idxs = []

    for word in negation_words:
        if word in tokens:
            idx = tokens.index(word)
            negation_idxs.append(idx)

    for word in positive_words:
        if word in tokens:
            idx = tokens.index(word)
            pos_words_idxs.append(idx)
        
    print(negation_idxs, pos_words_idxs)
    for idx in negation_idxs:
        if ((idx+1) in pos_words_idxs) or ((idx+2) in pos_words_idxs) or ((idx+3) in pos_words_idxs):
            print("Negacija in pozitivna beseda sta blizu")


def load_data(path, sample_size=None, context_size=2):
    '''
    Currently we add all strings(news text) to text_list
    and than perform tokenization via skleran tfidf,
    which is using our preproces_data function to tokenize text.

    For other cases, the data processing flow may be different.
    '''
    # assemble complete list of (tokens, sentiment)
    data = []

    nlp = stanza.Pipeline(lang='sl',
                          processors='tokenize,mwt,pos,lemma,depparse',
                          tokenize_pretokenized=True,
                          verbose=0)

    for file in list(path.glob("*.tsv"))[:sample_size]:
        with open(file, encoding="utf8") as f:

            text, df = text_from_tsv(f)

            tokens = list(df["token"].values)
            print(file)
           
            doc = nlp([tokens])
            lemmas = [word.lemma for word in doc.sentences[0].words]
            heads = [word.head for word in doc.sentences[0].words]
            df["token"] = lemmas
            df["heads"] = heads

            #break

            data += document_context_extraction(df, context_size=context_size)
            # data += document_context_based_on_dependencies(df, context_size=context_size)
            
    #return df
    input_data = list(zip(*data))

    X = input_data[0]
    y_binarized = input_data[1]
    y_original = input_data[2]

    X = np.array(X)
    y = np.vstack([y_binarized, y_original]).T

    return X, y


def load_data_2(path, dep_parse, context_adv,
                whole_sentence, sample_size=None, context_size=2):
    '''
    Currently we add all strings(news text) to text_list
    and than perform tokenization via skleran tfidf,
    which is using our preproces_data function to tokenize text.

    For other cases, the data processing flow may be different.
    '''

    data = []
    for file in list(path.glob("*.csv"))[:sample_size]:
        df = pd.read_csv(file)

        # data += document_context_extraction(df, context_size=context_size)
        data += document_context_extraction(df,
                                            dep_parse,
                                            context_adv,
                                            whole_sentence,
                                            context_size=context_size)

    input_data = list(zip(*data))

    X = input_data[0]
    y_binarized = input_data[1]
    y_original = input_data[2]

    X = np.array(X)
    y = np.vstack([y_binarized, y_original]).T

    return X, y


if __name__ == '__main__':
    raw_data_path = Path("../data/raw/")

    data = load_data(raw_data_path)
    print(data)
