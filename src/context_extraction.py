import numpy as np
import string

from nltk.corpus import stopwords


def preprocess_data_2(tokens):
    '''
    Removes string punctuations and stop words from `tokens`.
    '''
    tokens = [token.lower() for token in tokens if token not in string.punctuation]
    stop_words = stopwords.words("slovene")
    tokens = [token for token in tokens if token not in stop_words]

    return tokens


def add_tokens(entity, tokens, entity_tokens_dict):
    """
    Adds new `tokens` to `entity` in `entity_tokens_dict`.
    """
    if entity not in entity_tokens_dict.keys():
        entity_tokens_dict[entity] = tokens
    else:
        entity_tokens_dict[entity] += tokens


def context_words(text, contex_size):
    """
    Extracts surrounding words around each 'maska' word.
    'maska' repersents named entity in the text.
    """
    current_X = []
    #text = sentence_df["token"].values
    for idx in range(len(text)):
        if text[idx] == 'maska':
            if idx >= contex_size:
                if (idx + contex_size) < len(text):
                    current_X += text[idx-contex_size:idx+contex_size+1]
                else:
                    current_X += text[idx-contex_size:]
            elif idx < contex_size:
                if (idx + contex_size) < len(text):
                    current_X += text[:idx+contex_size+1]
                else:
                    # debug 
                    #print(text)
                    current_X += text
    current_X = [curr for curr in current_X if curr != 'maska']

    return current_X


def context_words_advanced(sentence_df, contex_size):
    """
    Extracts surrounding words around each 'maska' word.
    'maska' repersents named entity in the text.
    """
    def check_pos_tags(candidates_df, tags_list=["VERB", "ADJ", "ADV"]):
        tokens = []
        for tag in tags_list:
            tokens += list(candidates_df[candidates_df["upos"] == tag]["token"].values)

        return tokens

    current_X = []
    n = len(sentence_df)
    #text = sentence_df["token"].values
    for idx in range(n):
        if sentence_df.iloc[idx]["token"] == 'maska':
            if idx >= contex_size:
                if (idx + contex_size) < n:
                    possible_context = sentence_df.iloc[idx-contex_size:idx+contex_size+1]
                    current_X += check_pos_tags(possible_context)
                else:
                    possible_context = sentence_df.iloc[idx-contex_size:]
                    current_X += check_pos_tags(possible_context)
            elif idx < contex_size:
                if (idx + contex_size) < n:
                    possible_context = sentence_df.iloc[:idx+contex_size+1]
                    current_X += check_pos_tags(possible_context)
                else:
                    # debug 
                    #print(sentence_df)
                    possible_context = sentence_df
                    current_X += check_pos_tags(possible_context)
    current_X = [curr for curr in current_X if curr != 'maska']

    return current_X


def neighborhood_words(sentence_df, entities, entity_tokens_dict,
                       context_size, context_adv,
                       mask="maska"):
    """
    Extracts context words for all `entites` in `sentence_df`
    and adds them to `entity_tokens_dict`.
    """
    for entity in entities:
        sent = sentence_df.copy()

        # Assing `mask` value to current entity
        sent.loc[sent["idk_2"] == entity, "token"] = mask

        if context_adv:
            # advanced context extraction
            entity_tokens = context_words_advanced(sent, context_size)

        else:
            # preprocess tokens
            tokens = list(sent["token"].values)
            tokens_proc = preprocess_data_2(tokens)

            # extract context words and add them to dictionary
            entity_tokens = context_words(tokens_proc, context_size)

        add_tokens(entity, entity_tokens, entity_tokens_dict)


def contex_based_on_dependencies(df, entity):
    """
    Given the `entity` extract from `df` tokens
    that directly depend on it.

    DEVELOPMENT: Add second level of dependencies
    """

    # extract all idxes of entity
    entity_idxes = df.index[df['idk_2'] == entity].tolist()
    entity_idxes = np.array(entity_idxes) + 1

    entity_tokens = []
    # extract tokens of which heads point to one of entity idxes
    # for head, token in df[["heads", "token"]].values:
    #     if head in entity_idxes:
    #         entity_tokens.append(token)

    # DEVELOP

    first_order_dependents = []
    for idx, row in enumerate(df[["heads", "token"]].values):
        if row[0] in entity_idxes:
            entity_tokens.append(row[1])
            first_order_dependents.append(idx + 1) # idx + 1 is word index according to stanza

    for head, token in df[["heads", "token"]].values:
        if head in first_order_dependents:
            entity_tokens.append(token)

    return entity_tokens


def neighborhood_words_2(df, entities, entity_tokens_dict):

    """
    Extracts context words for all `entites` in `sentence`
    and adds them to `entity_tokens_dict`, based on dependency parsing.
    """
    for entity in entities:
        entity_tokens = contex_based_on_dependencies(df, entity)
        entity_tokens = preprocess_data_2(entity_tokens)
        add_tokens(entity, entity_tokens, entity_tokens_dict)


def sentence_contex_extraction(sentence_tokens, entity_tokens_dict, 
                               entites_list, context_size, 
                               context_adv, whole_sentence):
    """
    sentence_token -> set of tokens from one sentence, dataframe
    entity_tokens_dict -> entity -> tokens dictionary

    """

    sentence_array = sentence_tokens.values
    sentence_entities = list(np.unique(sentence_array[:, 1]))

    try:
        sentence_entities.remove('_')
    except:
        pass

    if sentence_entities is None:
        # ignore sentences with no entity

        # for entity in entites_list:
        #     entity_tokens = list(sentence_arr[:, 0])
        #     entity_tokens_proc = preprocess_data_2(entity_tokens)
        #     add_tokens(entity, entity_tokens_proc, entity_tokens_dict)
        pass

    elif len(sentence_entities) == 1:
        # sentence with one entity, take whole sentence as context
        if whole_sentence:
            entity_tokens = list(sentence_array[:, 0])
            entity_tokens_proc = preprocess_data_2(entity_tokens)
            add_tokens(sentence_entities[0], entity_tokens_proc, entity_tokens_dict)
        else:
            neighborhood_words(sentence_tokens, sentence_entities,
                               entity_tokens_dict, context_size,
                               context_adv, mask="maska")
    else:
        # sentence with more than one entity, extract neighborhood
        # words for each of them
        neighborhood_words(sentence_tokens, sentence_entities,
                           entity_tokens_dict, context_size,
                           context_adv, mask="maska")

        # this setting was for sentence based dependenies, 
        # for now we use dependecies on whole text

        # neighborhood_words_2(sentence_tokens, sentence_entities,
        #                      entity_tokens_dict, mask="maska")

    return entity_tokens_dict


def document_context_based_on_dependencies(sentence_tokens, 
                                           entity_tokens_dict, entites_list):
    """
    Extract context words for each entity

    """

    sentence_arr = sentence_tokens.values
    sentence_entities = list(np.unique(sentence_arr[:, 1]))

    try:
        sentence_entities.remove('_')
    except:
        pass

    neighborhood_words_2(sentence_tokens, sentence_entities,
                         entity_tokens_dict, mask="maska")

    return entity_tokens_dict


def document_context_extraction(df, dep_parse, context_adv, whole_sentence, context_size=2):
    """
    Returns a list of tuples: 
    [(context_words_1, sentiment_1), (context_words_2, sentiment_2) ....],

    Arg:
    df -- DataFrame produced by text_from_tsv

    """

    # extract all entities in text and their context
    sent = df[(df["sentiment"] != "_") & (df["idk_2"] != "_")]
    entity_sentiment_dict = dict(zip(sent["idk_2"].values, sent["sentiment"].values))
    entity_org_sentiment_dict = dict(zip(sent["idk_2"].values, sent["orig_sentiment"].values))

    # find sentences split for processing of individual sentences
    splits = df[df['token'] == "."].index
    text_len = len(df)
    entity_tokens_dict = {}

    token_entity_df = df[["token", "idk_2", "heads", "upos"]]
    entities_list = entity_sentiment_dict.keys()

    # extract context word from dependencies on whole text
    if dep_parse:
        neighborhood_words_2(token_entity_df, entities_list,
                             entity_tokens_dict)

    if True:
        # first sentence
        sentence_tokens = token_entity_df[0:splits[0] + 1]
        entity_tokens_dict = sentence_contex_extraction(sentence_tokens,
                                                        entity_tokens_dict,
                                                        entities_list,
                                                        context_size,
                                                        context_adv,
                                                        whole_sentence)

        # following sentences
        for i in range(len(splits) - 1):
            sentence_tokens = token_entity_df[splits[i] + 1:splits[i + 1] + 1]
            entity_tokens_dict = sentence_contex_extraction(sentence_tokens,
                                                            entity_tokens_dict,
                                                            entities_list,
                                                            context_size,
                                                            context_adv,
                                                            whole_sentence)

        # last sentence only in case there is
        # missing dot at the end of text
        if splits[-1] != text_len:
            sentence_tokens = token_entity_df[splits[-1] + 1:]
            entity_tokens_dict = sentence_contex_extraction(sentence_tokens,
                                                            entity_tokens_dict,
                                                            entities_list,
                                                            context_size,
                                                            context_adv,
                                                            whole_sentence)

    # construct [(context_words_1, sentiment_1), (context_words_2, sentiment_2) ....]
    tokens_sentiment_tuple_list = []
    for entity in entity_sentiment_dict.keys():
        tokens_sentiment_tuple_list.append((entity_tokens_dict[entity],
                                            entity_sentiment_dict[entity],
                                            entity_org_sentiment_dict[entity]))

    return tokens_sentiment_tuple_list
