import numpy as np
import matplotlib.pyplot as plt


# sentiment distribution
x = np.arange(5)
plt.bar(x, height=[30,1801,10869,1705,24])
plt.xticks(x, ['Very negative','Negative','Neutral','Positive','Very positive'])
plt.xlabel('Sentiment')
plt.ylabel('N')
plt.show()


# n-grams
x = np.arange(3)
y1 = [54.7,58.3,57.7]
y2 = [57.7,60.7,57.0]
y3 = [53.7,59.0,53.7]

fig = plt.figure()
ax = fig.add_subplot(111)
r1 = ax.bar(x-0.2, y1, width=0.2, color='b', align='center')
r2 = ax.bar(x, y2, width=0.2, color='g', align='center')
r3 = ax.bar(x+0.2, y3, width=0.2, color='r', align='center')
plt.xlabel('N-grams')
plt.ylabel('Average F1 score')
ax.set_xticks(x)
ax.set_xticklabels(('unigrams','uni+bigrams','uni+bi+trigrams'))
ax.legend((r1,r2,r3), ('CNB', 'MNB', 'LR'))
plt.show()