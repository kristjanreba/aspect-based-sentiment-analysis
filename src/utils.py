import sys
import lightgbm as lgb
import numpy as np

from sklearn.model_selection import KFold
from sklearn.metrics import precision_recall_fscore_support
from sklearn.naive_bayes import MultinomialNB, ComplementNB
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score

from imblearn.pipeline import Pipeline
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler
from collections import Counter

sys.path.append("..")
from src.process_data import doc2vec


def SVM(X_train, y_train, X_test):
    model = SVC(C=1e4,
                kernel='rbf',
                cache_size=1000)  

    model.fit(X_train, y_train)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def LR(X_train, y_train, X_test):
    model = LogisticRegression(max_iter=250)         

    model.fit(X_train, y_train)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def RandomForest(X_train, y_train, X_test):
    model = RandomForestClassifier()        

    model.fit(X_train, y_train)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def DNN(X_train, y_train, X_test):
    model = MLPClassifier(hidden_layer_sizes=(40, 20,),
                            activation='relu',
                            solver='adam',
                            alpha=1e-5,
                            batch_size='auto',
                            max_iter=int(1e5),
                            n_iter_no_change=20)
    model.fit(X_train, y_train)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def LightGBM(X_train, y_train, X_test):

    train_dataset = lgb.Dataset(X_train, label=y_train)

    num_iterations = 50 # to je število dreves
    param = {#'learning_rate: 0.1'
             #'num_leaves': 24,
             #'max_depth': 7,
             'objective': 'multiclass',
             'num_class': 3,
             'metric': 'multi_logloss'#,
             #'scale_pos_weight': 1/pos_ratio
        }

    model = lgb.train(param, train_dataset, 40)
    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return np.argmax(y_train_predict, axis=1), np.argmax(y_test_predict, axis=1)


def MNB(X_train, y_train, X_test):

    clf = MultinomialNB()
    clf.fit(X_train, y_train)

    y_train_predict = clf.predict(X_train)
    y_test_predict = clf.predict(X_test)

    return y_train_predict, y_test_predict


def NB(X_train, y_train, X_test):

    clf = ComplementNB()
    clf.fit(X_train, y_train)

    y_train_predict = clf.predict(X_train)
    y_test_predict = clf.predict(X_test)

    return y_train_predict, y_test_predict


def lexicon_classifier(dummy_1, dummy_2, X):
    def count_word(lexicon, words):
        n = 0
        for word in words:
            if word in lexicon:
                n += 1
        return n

    f = open("../pos_neg_lexicon/positive_words_lemmas.txt", encoding="utf-8")
    pos_list = [word.strip() for word in f.readlines()]
    f.close()

    f = open("../pos_neg_lexicon/negative_words_lemmas.txt", encoding="utf-8")
    neg_list = [word.strip() for word in f.readlines()]
    f.close()

    pred = []
    
    for words in X:
        n_pos = count_word(pos_list, words)
        n_neg = count_word(neg_list, words)
        
        if n_pos == n_neg:   # netural
            pred.append(0)
        elif n_pos > n_neg:  # positive
            pred.append(1)
        else:                 # negative
            pred.append(2)
       
    dummy_return = []
    return [], pred


def tf_idf(X_train, X_test):

    def dummy_fun(x):
        return x

    tfidf = TfidfVectorizer(
                            analyzer='word',
                            tokenizer=dummy_fun,
                            preprocessor=dummy_fun,
                            token_pattern=None)  

    X_train_tf = tfidf.fit_transform(X_train)
    X_test_tf = tfidf.transform(X_test)

    return X_train_tf, X_test_tf


def n_gram(X_train, X_test, ngram_range):

    def dummy_fun(x):
        return x

    ngram = CountVectorizer(
                            ngram_range=ngram_range,
                            analyzer='word',
                            tokenizer=dummy_fun,
                            preprocessor=dummy_fun,
                            token_pattern=None,
                            binary=True,
                            dtype=np.float32)   

    X_train_g = ngram.fit_transform(X_train)
    X_test_g = ngram.transform(X_test)

    return X_train_g, X_test_g


def resampling_scheme(X, y):
    # summarize class distribution
    counter = Counter(y)
    #print(counter)

    # define pipeline
    over_dict =  {1: 3000, 2: 3000}
    over = SMOTE(sampling_strategy=over_dict)

    # under_dict = {0: 10000}
    # under = RandomUnderSampler(sampling_strategy=under_dict)
    steps = [('o', over)]#, ('u', under)]
    pipeline = Pipeline(steps=steps)

    # transform the dataset

    X_n, y_n = pipeline.fit_resample(X, y)
    # X_n, y_n = pipeline.fit_resample(X.todense(), y)

    # summarize the new class distribution
    counter = Counter(y_n)
    #print(counter)
    return X_n, y_n


def to_binary(x):
    return x != 0


def cross_valid(X_cross, y_cross, model_name):
    results_cross = []
    results_cross_weigted = []
    k_folds = KFold(n_splits=10, shuffle=True)
    for train, test in k_folds.split(X_cross):
        train_XX, test_XX = X_cross[train], X_cross[test]
        train_yy, test_yy = y_cross[train], y_cross[test]

        # lexicon approach
        # y_train_predict, y_test_predict = model_name(train_XX, train_yy, 
        #                                              test_XX)

        # X_train_em, X_test_em = doc2vec(train_XX, test_XX)
        # y_train_predict, y_test_predict = model_name(X_train_em, train_yy, 
        #                                              X_test_em)

        # X_train, X_test = n_gram(train_XX, test_XX)
        # y_train_predict, y_test_predict = model_name(X_train, train_yy[:, 0], X_test)

        # scaler = preprocessing.StandardScaler()
        # train_XX = scaler.fit_transform(train_XX.todense())
        # test_XX = scaler.fit_transform(test_XX.todense())
        
        
        # y_train_predict, y_test_predict = model_name(train_XX, train_yy, test_XX)
        
        if True:
            X_n, y_n = resampling_scheme(train_XX, train_yy)
        else:
            X_n, y_n = train_XX, train_yy

        y_train_predict, y_test_predict = model_name(X_n, y_n, test_XX)
        
        scores = precision_recall_fscore_support(test_yy, y_test_predict, zero_division=0)
        scores_weighted = precision_recall_fscore_support(test_yy, y_test_predict, average='weighted')
        
        results_cross.append(scores)
        results_cross_weigted.append(scores_weighted)




        """ Zelo negativno/pozitivno """
        # for idx in range(test_yy.shape[0]):
        #     if "Zelo" in test_yy[idx, 1]:
        #         print(test_XX[idx])
        #         print("Predikcija: {}".format(y_test_predict[idx]))
        #         print("Oznaka: {}".format(test_yy[idx, :]))

    precisions = [score[0]*100 for score in results_cross] 
    recalls = [score[1]*100 for score in results_cross] 
    f1 = [score[2]*100 for score in results_cross] 

    mean_precision = np.round(np.mean(precisions, axis=0), 2)
    mean_recall = np.round(np.mean(recalls, axis=0), 2)
    mean_f1 = np.round(np.mean(f1, axis=0), 2)

    std_dev_precision = np.round(np.std(precisions, axis=0), 2)
    std_dev_recall = np.round(np.std(recalls, axis=0), 2)
    std_dev_f1 = np.round(np.std(f1, axis=0), 2)

    print("Cross validaton results:")
    print("Mean precision: " + str(mean_precision) + " +/- " + str(std_dev_precision))
    print("Mean recall: " + str(mean_recall) + " +/- " + str(std_dev_recall))
    print("Mean f1: " + str(mean_f1) + " +/- " + str(std_dev_f1))

    print("Mean f1 score for all classes: {} \pm {}".format(np.round(np.mean(mean_f1), 1),
                                                            np.round(np.mean(std_dev_f1), 1)))


    # weigted f1 scores, currently not used
    
    # precisions_w = [score[0]*100 for score in results_cross_weigted]
    # recalls_w = [score[1]*100 for score in results_cross_weigted]
    # f1_w = [score[2]*100 for score in results_cross_weigted]

    # mean_precision_w = np.round(np.mean(precisions_w, axis=0), 2)
    # mean_recall_w = np.round(np.mean(recalls_w, axis=0), 2)
    # mean_f1_w = np.round(np.mean(f1_w, axis=0), 2)

    # std_dev_precision_w = np.round(np.std(precisions_w, axis=0), 2)
    # std_dev_recall_w = np.round(np.std(recalls_w, axis=0), 2)
    # std_dev_f1_w = np.round(np.std(f1_w, axis=0), 2)

    # print("Cross validaton results:")
    # print("Mean weighted precision: " + str(mean_precision_w) + " +/- " + str(std_dev_precision_w))
    # print("Mean weighted recall: " + str(mean_recall_w) + " +/- " + str(std_dev_recall_w))
    # print("Mean weighted f1: " + str(mean_f1_w) + " +/- " + str(std_dev_f1_w))

    #return np.mean(mean_f1_w), np.std(std_dev_f1_w)
    return np.mean(mean_f1), np.std(std_dev_f1)
