# Aspect-based sentiment analysis (Jakob Bevc, Kristjan Reba)

This repository contains executable code for seminar work at natural language processing course. We made a research on aspect-based sentiment analysis. The results are presented in the `final_report` document. 

## Requirements installation

To install all the necessary packages use the following command:

```bash
pip install -r requirements.txt
```

## Usage
To replicate our experiment we provided `simple_run.py` script in which you can specify the parameters for entity context extraction and choose machine learning model of your choice. With usage of the bellow command 10-fold cross validation will be made for parameters you specify. Usually the loading/context extraction takes around 4 minutes. 

```bash
python simple_run.py
```
