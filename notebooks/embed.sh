#!/bin/bash

if [ -z ${LASER+x} ] ; then
  echo "Please set the environment variable 'LASER'"
  exit
fi

if [ $# -ne 3 ] ; then
  echo "usage embed.sh input-file language output-file"
  exit
fi

ifile=$1
lang=$2
ofile=$3

# encder
model_dir="${LASER}/models"
encoder="${model_dir}/bilstm.93langs.2018-12-26.pt"
bpe_codes="${model_dir}/93langs.fcodes"

cat $ifile \
  | python3 ${LASER}/source/embed.py \
    --encoder ${encoder} \
    --token-lang ${lang} \
    --bpe-codes ${bpe_codes} \
    --output ${ofile} \
    --verbose
