## Experiment 1
### Parameters:
    - advanced context extraction (context_size = 2)
    - 2-level dependency parsing
    - bigram and unigrams
    
### Results:
    Evaluation for model: NB
    Cross validaton results:
    Mean precision: [85.63 28.07 31.15] +/- [1.92 1.94 2.36]
    Mean recall: [67.96 44.26 53.26] +/- [1.01 3.97 5.27]
    Mean f1: [75.78 34.27 39.24] +/- [1.21 2.04 2.84]
    Mean f1 score for all classes: 49.8 \pm 2.0

    Evaluation for model: MNB
    Cross validaton results:
    Mean precision: [82.21 40.91 42.68] +/- [1.1  4.14 4.04]
    Mean recall: [87.25 30.49 36.88] +/- [1.08 2.75 2.6 ]
    Mean f1: [84.65 34.84 39.47] +/- [0.72 2.8  2.59]
    Mean f1 score for all classes: 53.0 \pm 2.0

    Evaluation for model: LR
    Cross validaton results:
    Mean precision: [79.66 45.02 47.8 ] +/- [1.72 5.   4.52]
    Mean recall: [93.62 19.71 22.48] +/- [0.91 4.29 2.78]
    Mean f1: [86.07 27.22 30.54] +/- [1.21 4.77 3.37]
    Mean f1 score for all classes: 47.9 \pm 3.1
  
