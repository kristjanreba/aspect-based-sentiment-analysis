# Dokument z zapiski o že narejenem in trenutnem delu

## Zapiski glede trenutne implementacije
- Doc2Vec je potrebno predprocesiranje tesktsa in odstranjevanje stop-wordov itd.? V člankih omenjajo, da to ni potrebno, da se ohrani kontekst.



### Rezultati za n-bližnji(3) 
    `Rezultati(lexicon based klasifikator):`
        Mean precision: [0.81 0.17 0.25] +/- [0.01 0.01 0.02]
        Mean recall: [0.54 0.44 0.38] +/- [0.02 0.01 0.03]
        Mean f1: [0.65 0.25 0.30] +/- [0.01 0.01 0.02]


### Testiranje novo implementacije konteksta 
    - če je v stavku samo ena entitea vzami celoten stavek kot kontekst te entitete
    - če je v stavku več enitet vzemi kot kontekst n-bližnji(v tem eksperimentu 3) besed vsake entitete
    `Rezultati(lexicon based klasifikator):`
        Mean precision: [0.81 0.18 0.26] +/- [0.01 0.01 0.02]
        Mean recall: [0.59 0.42 0.37] +/- [0.02 0.01 0.03]
        Mean f1: [0.68 0.25 0.31] +/- [0.01 0.01 0.02]

### Nova implementacija konteksta z n-gram embeddingom in Multinomial NB modelom
Najboljši rezultat z unigrami, bigrami in več poslabša rezultat.
`Rezultati (unigrami, notebook: jb-NB-model-n-grams)`

    Mean precision: [0.83 0.4  0.41] +/- [0.01 0.04 0.02]
    Mean recall: [0.84 0.35 0.43] +/- [0.01 0.03 0.03]
    Mean f1: [0.83 0.37 0.42] +/- [0.   0.03 0.02]
    Mean f1 score for all classes: 0.5399999999999999

NAROBE, OVERSAMPLING NA CELOTNI MNOŽICI: Uporaba SMOTE oversamplinga in undersamplinga izboljša rezultate:
Distrubicije razredov prej in potem:
```
Counter({'0': 11229, '2': 1856, '1': 1739})
Counter({'0': 8000, '1': 3000, '2': 3000})
```

    Mean precision: [0.77 0.58 0.56] +/- [0.01 0.03 0.02]
    Mean recall: [0.7  0.62 0.66] +/- [0.01 0.03 0.02]
    Mean f1: [0.73 0.6  0.61] +/- [0.01 0.03 0.02]
    Mean f1 score for all classes: 0.6466666666666666 (MNB, NB even better)


10-fold cross validaton, z oversamplingom samo na učni množici in uporabo `stanza` lemmatizatorja:

- undersampling ni potreben oz poslabša rezultate
- potrebno testiranje delovanja, če mamo unigrame z counti ali samo binarninmi vrednostmi (NB in MNB dela rahklo boljše LR dela isto)
- testirat unigrame, bigrame, trigrame ...


10-fold cross validaton, z oversamplingom samo na učni množici in uporabo `stanza` lemmatizatorja, unigrami s štetjem pojavitev:

    Evaluation for model: NB
    Counter({0: 10089, 2: 1686, 1: 1566})
    Counter({0: 10089, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.88 0.31 0.32] +/- [0.01 0.02 0.02]
    Mean recall: [0.63 0.54 0.64] +/- [0.01 0.04 0.03]
    Mean f1: [0.73 0.39 0.42] +/- [0.01 0.02 0.02]
    Mean f1 score for all classes: 0.5133333333333333

    Evaluation for model: MNB
    Counter({0: 10076, 2: 1685, 1: 1580})
    Counter({0: 10076, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.86 0.35 0.37] +/- [0.01 0.02 0.04]
    Mean recall: [0.75 0.47 0.54] +/- [0.02 0.03 0.03]
    Mean f1: [0.8  0.4  0.44] +/- [0.01 0.02 0.03]
    Mean f1 score for all classes: 0.5466666666666667

    Evaluation for model: LR
    Counter({0: 10128, 2: 1658, 1: 1555})
    Counter({0: 10128, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.83 0.41 0.45] +/- [0.01 0.03 0.04]
    Mean recall: [0.86 0.37 0.41] +/- [0.01 0.02 0.06]
    Mean f1: [0.85 0.38 0.43] +/- [0.01 0.02 0.04]
    Mean f1 score for all classes: 0.5533333333333333

10-fold cross validaton, z oversamplingom samo na učni množici in uporabo `stanza` lemmatizatorja, unigrami s binarnimi pojavitvami:

    Evaluation for model: NB
    Counter({0: 10123, 2: 1669, 1: 1549})
    Counter({0: 10123, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.87 0.32 0.32] +/- [0.01 0.02 0.02]
    Mean recall: [0.65 0.54 0.63] +/- [0.01 0.04 0.05]
    Mean f1: [0.74 0.4  0.43] +/- [0.01 0.02 0.02]
    Mean f1 score for all classes: 0.5233333333333333

    Evaluation for model: MNB
    Counter({0: 10099, 2: 1678, 1: 1564})
    Counter({0: 10099, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.85 0.4  0.42] +/- [0.   0.04 0.03]
    Mean recall: [0.82 0.42 0.49] +/- [0.01 0.03 0.02]
    Mean f1: [0.83 0.41 0.45] +/- [0.01 0.03 0.02]
    Mean f1 score for all classes: 0.5633333333333334

    Evaluation for model: LR
    Counter({0: 10093, 2: 1674, 1: 1574})
    Counter({0: 10093, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.83 0.42 0.48] +/- [0.02 0.05 0.05]
    Mean recall: [0.87 0.36 0.41] +/- [0.01 0.05 0.04]
    Mean f1: [0.85 0.39 0.44] +/- [0.01 0.05 0.04]
    Mean f1 score for all classes: 0.5599999999999999


10-fold cross validaton, z oversamplingom samo na učni množici in uporabo `stanza` lemmatizatorja, unigrami in bigrami s binarnimi pojavitvami:

    Evaluation for model: NB
    Counter({0: 10113, 2: 1655, 1: 1573})
    Counter({0: 10113, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.9  0.34 0.36] +/- [0.01 0.03 0.03]
    Mean recall: [0.66 0.63 0.64] +/- [0.01 0.04 0.05]
    Mean f1: [0.76 0.44 0.46] +/- [0.01 0.03 0.03]
    Mean f1 score for all classes: 0.5533333333333333

    Evaluation for model: MNB
    Counter({0: 10134, 2: 1650, 1: 1557})
    Counter({0: 10134, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.86 0.44 0.42] +/- [0.01 0.03 0.03]
    Mean recall: [0.8  0.48 0.55] +/- [0.01 0.04 0.02]
    Mean f1: [0.83 0.46 0.48] +/- [0.01 0.03 0.03]
    Mean f1 score for all classes: `0.59`

    Evaluation for model: LR
    Counter({0: 10109, 2: 1652, 1: 1580})
    Counter({0: 10109, 1: 3000, 2: 3000})
    Cross validaton results:
    Mean precision: [0.82 0.51 0.53] +/- [0.01 0.03 0.04]
    Mean recall: [0.91 0.32 0.34] +/- [0.01 0.02 0.04]
    Mean f1: [0.86 0.4  0.41] +/- [0.01 0.02 0.04]
    Mean f1 score for all classes: 0.5566666666666666

10-fold cross validaton, z oversamplingom samo na učni množici in uporabo `stanza` lemmatizatorja, unigrami in bigrami s števnimi pojavitvami:

    Evaluation for model: NB
    Cross validaton results:
    Mean precision: [0.89 0.33 0.35] +/- [0.01 0.03 0.02]
    Mean recall: [0.64 0.62 0.66] +/- [0.01 0.05 0.02]
    Mean f1: [0.74 0.43 0.45] +/- [0.01 0.04 0.02]
    Mean f1 score for all classes: 0.5399999999999999

    Evaluation for model: MNB
    Cross validaton results:
    Mean precision: [0.87 0.38 0.37] +/- [0.01 0.02 0.05]
    Mean recall: [0.74 0.51 0.59] +/- [0.02 0.03 0.04]
    Mean f1: [0.8  0.43 0.46] +/- [0.01 0.02 0.04]
    Mean f1 score for all classes: 0.5633333333333334

    Evaluation for model: LR
    Cross validaton results:
    Mean precision: [0.82 0.49 0.51] +/- [0.01 0.03 0.04]
    Mean recall: [0.9  0.34 0.37] +/- [0.01 0.03 0.02]
    Mean f1: [0.86 0.4  0.42] +/- [0.01 0.03 0.02]
    Mean f1 score for all classes: 0.5599999999999999

10-fold cross validaton, z oversamplingom samo na učni množici in uporabo `stanza` lemmatizatorja, unigrami, bigrami, trigrami s binarnimi pojavitvami:

    Evaluation for model: NB
    Cross validaton results:
    Mean precision: [0.89 0.32 0.34] +/- [0.01 0.02 0.02]
    Mean recall: [0.63 0.62 0.65] +/- [0.01 0.03 0.04]
    Mean f1: [0.74 0.43 0.45] +/- [0.01 0.02 0.02]
    Mean f1 score for all classes: 0.5399999999999999

    Evaluation for model: MNB
    Cross validaton results:
    Mean precision: [0.87 0.38 0.38] +/- [0.01 0.03 0.02]
    Mean recall: [0.74 0.51 0.59] +/- [0.01 0.05 0.04]
    Mean f1: [0.8  0.44 0.46] +/- [0.01 0.03 0.02]
    Mean f1 score for all classes: 0.5666666666666667

    Evaluation for model: LR
    Cross validaton results:
    Mean precision: [0.83 0.5  0.52] +/- [0.01 0.06 0.04]
    Mean recall: [0.9  0.34 0.37] +/- [0.01 0.05 0.02]
    Mean f1: [0.86 0.41 0.43] +/- [0.01 0.05 0.02]
    Mean f1 score for all classes: 0.5666666666666667

10-fold cross validaton, z oversamplingom samo na učni množici in uporabo `stanza` lemmatizatorja, tf-idf:

    Evaluation for model: NB
    Cross validaton results:
    Mean precision: [0.84 0.34 0.37] +/- [0.01 0.03 0.02]
    Mean recall: [0.75 0.45 0.47] +/- [0.01 0.04 0.04]
    Mean f1: [0.79 0.38 0.41] +/- [0.01 0.03 0.03]
    Mean f1 score for all classes: 0.5266666666666666

    Evaluation for model: MNB
    Cross validaton results:
    Mean precision: [0.77 0.74 0.72] +/- [0.01 0.07 0.08]
    Mean recall: [0.99 0.1  0.07] +/- [0.   0.01 0.02]
    Mean f1: [0.87 0.18 0.13] +/- [0.01 0.02 0.03]
    Mean f1 score for all classes: 0.39333333333333337

    Evaluation for model: LR
    Cross validaton results:
    Mean precision: [0.8  0.49 0.51] +/- [0.01 0.06 0.03]
    Mean recall: [0.93 0.22 0.27] +/- [0.01 0.05 0.02]
    Mean f1: [0.86 0.3  0.35] +/- [0.01 0.06 0.02]
    Mean f1 score for all classes: 0.5033333333333333
## Defense 2 notes:
   
- bolj natančno utemeljiti, kako delujejo posamezni pristopi: dopisati kakšni so parametri in testirati za različne možnosti
- izboljšana analiza: primerjava s sentinews datasetom, primerjava entitet glede na sentiment stavka, celotne novice (npr. če vemo, da je celoten dokument pozitiven, je potem bolj verjetno, da je več entitet pozitivnih; ali če se neka beseda pojavlja bolj v pozitinih stavkih, je potem bolj pozitivna?)
- preskusite tudi označevanje skladenjskih vlog (semantic role labeling) za pridobitev dodatnih značilk glede kontekstnih besed.
- preglejte, kako dobro se označujejo primeri, ki so zelo negativni in zelo pozitivni


# TODO
- primerjava sentence level sentimenta z sentimentom entitet (https://www.clarin.si/repository/xmlui/handle/11356/1110)
- označevanje negacij
- preveri označevanje primerov, ki so zelo negativnih/pozitivni (osnova narejejna)
- evalvacija več ML modelov in primerjava za različne n-gram, modele itd..
- oversampling



## Članki
    - multi_entity_seniment_analysis (nič pametnega, delajo na podlagi sosdenjih besed (2 levo 2 desno) in potem lexicon..)
    - entitiy specific sentiment analysis
        - Pravilo 1: V stavkih, kjer je samo ena entiteta vzamejo celoten stavek kot kontekst te entitete.
