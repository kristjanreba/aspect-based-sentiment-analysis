# 1 Optimal
```
Parameters:
    local_context = 2
    dependencies on whole text
    sentence with only one entity, add all tokens
    add sentence without entities to contex of all sentences
```

## Results
`lexicon clasificator`
```
Cross validaton results:
Mean precision: [0.81 0.18 0.26] +/- [0.01 0.01 0.03]
Mean recall: [0.56 0.47 0.34] +/- [0.01 0.03 0.03]
Mean f1: [0.66 0.26 0.29] +/- [0.01 0.02 0.03]
Mean f1 score for all classes: 0.4033333333333333
```



`Unigrams`

```
Evaluation for model: NB
Cross validaton results:
Mean precision: [0.88 0.35 0.35] +/- [0.01 0.03 0.02]
Mean recall: [0.67 0.56 0.65] +/- [0.02 0.02 0.03]
Mean f1: [0.76 0.43 0.45] +/- [0.01 0.03 0.02]
Mean f1 score for all classes: 0.5466666666666666

Evaluation for model: MNB
Cross validaton results:
Mean precision: [0.86 0.43 0.42] +/- [0.01 0.03 0.02]
Mean recall: [0.8  0.47 0.54] +/- [0.01 0.03 0.03]
Mean f1: [0.83 0.45 0.47] +/- [0.01 0.03 0.02]
Mean f1 score for all classes: 0.5833333333333334

Evaluation for model: LR
Cross validaton results:
Mean precision: [0.84 0.46 0.48] +/- [0.01 0.04 0.02]
Mean recall: [0.87 0.4  0.43] +/- [0.01 0.03 0.05]
Mean f1: [0.86 0.42 0.45] +/- [0.01 0.02 0.03]
Mean f1 score for all classes: 0.5766666666666667
```




`Unigrams and Bigrams`
```
Evaluation for model: NB
Cross validaton results:
Mean precision: [0.91 0.37 0.37] +/- [0.01 0.02 0.03]
Mean recall: [0.67 0.65 0.69] +/- [0.01 0.04 0.04]
Mean f1: [0.77 0.47 0.49] +/- [0.01 0.02 0.03]
Mean f1 score for all classes: 0.5766666666666667

Evaluation for model: MNB
Cross validaton results:
Mean precision: [0.87 0.45 0.44] +/- [0.01 0.03 0.03]
Mean recall: [0.81 0.51 0.59] +/- [0.01 0.04 0.04]
Mean f1: [0.84 0.48 0.5 ] +/- [0.01 0.03 0.03]
Mean f1 score for all classes: 0.6066666666666666

Evaluation for model: LR
Cross validaton results:
Mean precision: [0.82 0.55 0.57] +/- [0.01 0.05 0.05]
Mean recall: [0.93 0.33 0.35] +/- [0.01 0.04 0.03]
Mean f1: [0.87 0.41 0.43] +/- [0.01 0.04 0.03]
Mean f1 score for all classes: 0.57
```


`Unigrams and Bigrams and Trigrams`
```
Evaluation for model: NB
Cross validaton results:
Mean precision: [0.93 0.3  0.36] +/- [0.01 0.02 0.03]
Mean recall: [0.57 0.74 0.7 ] +/- [0.01 0.04 0.03]
Mean f1: [0.71 0.43 0.47] +/- [0.01 0.03 0.03]
Mean f1 score for all classes: 0.5366666666666666

Evaluation for model: MNB
Cross validaton results:
Mean precision: [0.9  0.39 0.39] +/- [0.01 0.03 0.02]
Mean recall: [0.72 0.62 0.66] +/- [0.01 0.03 0.03]
Mean f1: [0.8  0.48 0.49] +/- [0.   0.02 0.02]
Mean f1 score for all classes: 0.59

Evaluation for model: LR
Cross validaton results:
Mean precision: [0.81 0.57 0.57] +/- [0.01 0.05 0.06]
Mean recall: [0.94 0.28 0.28] +/- [0.01 0.03 0.04]
Mean f1: [0.87 0.37 0.37] +/- [0.   0.03 0.04]
Mean f1 score for all classes: 0.5366666666666666

```



# 1 Without dependeny
```
Parameters:
    local_context = 2
    sentence with only one entity, add all tokens
    add sentence without entities to contex of all sentences
```
`lexicon clasificator`
```
Cross validaton results:
Mean precision: [0.81 0.18 0.26] +/- [0.01 0.02 0.02]
Mean recall: [0.56 0.47 0.34] +/- [0.02 0.03 0.03]
Mean f1: [0.66 0.26 0.29] +/- [0.01 0.02 0.02]
Mean f1 score for all classes: 0.4033333333333333
```



`Unigrams`

```
Evaluation for model: NB
Cross validaton results:
Mean precision: [0.89 0.34 0.35] +/- [0.01 0.02 0.01]
Mean recall: [0.67 0.56 0.66] +/- [0.01 0.05 0.02]
Mean f1: [0.76 0.43 0.45] +/- [0.01 0.03 0.01]
Mean f1 score for all classes: 0.5466666666666666

Evaluation for model: MNB
Cross validaton results:
Mean precision: [0.86 0.43 0.42] +/- [0.01 0.03 0.03]
Mean recall: [0.81 0.47 0.54] +/- [0.01 0.03 0.05]
Mean f1: [0.83 0.45 0.47] +/- [0.01 0.02 0.03]
Mean f1 score for all classes: 0.5833333333333334

Evaluation for model: LR
Cross validaton results:
Mean precision: [0.84 0.46 0.49] +/- [0.01 0.05 0.03]
Mean recall: [0.87 0.4  0.42] +/- [0.02 0.03 0.04]
Mean f1: [0.86 0.43 0.45] +/- [0.01 0.03 0.03]
Mean f1 score for all classes: 0.58
```

`Unigrams and Bigrams`
```
Evaluation for model: NB
Cross validaton results:
Mean precision: [0.9  0.37 0.37] +/- [0.01 0.02 0.03]
Mean recall: [0.67 0.65 0.68] +/- [0.01 0.04 0.03]
Mean f1: [0.77 0.47 0.48] +/- [0.01 0.02 0.03]
Mean f1 score for all classes: 0.5733333333333334

Evaluation for model: MNB
Cross validaton results:
Mean precision: [0.87 0.45 0.43] +/- [0.01 0.03 0.04]
Mean recall: [0.8  0.51 0.58] +/- [0.01 0.02 0.04]
Mean f1: [0.84 0.48 0.49] +/- [0.01 0.02 0.04]
Mean f1 score for all classes: 0.6033333333333333

Evaluation for model: LR
Cross validaton results:
Mean precision: [0.82 0.56 0.56] +/- [0.01 0.04 0.04]
Mean recall: [0.93 0.33 0.35] +/- [0.01 0.02 0.02]
Mean f1: [0.87 0.42 0.43] +/- [0.01 0.02 0.02]
Mean f1 score for all classes: 0.5733333333333334
```

`Unigrams and Bigrams and Trigrams`
```
Evaluation for model: NB
Cross validaton results:
Mean precision: [0.93 0.31 0.35] +/- [0.01 0.02 0.03]
Mean recall: [0.58 0.75 0.69] +/- [0.01 0.03 0.02]
Mean f1: [0.71 0.44 0.47] +/- [0.01 0.02 0.03]
Mean f1 score for all classes: 0.5399999999999999

Evaluation for model: MNB
Cross validaton results:
Mean precision: [0.9  0.39 0.39] +/- [0.01 0.02 0.02]
Mean recall: [0.71 0.62 0.67] +/- [0.01 0.03 0.03]
Mean f1: [0.8  0.48 0.5 ] +/- [0.01 0.02 0.02]
Mean f1 score for all classes: 0.5933333333333334

Evaluation for model: LR
Cross validaton results:
Mean precision: [0.81 0.56 0.59] +/- [0.   0.05 0.04]
Mean recall: [0.94 0.27 0.28] +/- [0.01 0.03 0.03]
Mean f1: [0.87 0.37 0.38] +/- [0.   0.03 0.03]
Mean f1 score for all classes: 0.54
```



# 1 Without dependeny, WITHOUT SENTENCE WITHOUT ENTITIYTS
```
Parameters:
    local_context = 2
    sentence with only one entity, add all tokens
   
```
`lexicon clasificator`
```

Cross validaton results:
Mean precision: [81.14 17.57 26.09] +/- [1.48 1.63 1.99]
Mean recall: [56.08 47.1  33.84] +/- [1.46 3.27 2.29]
Mean f1: [66.31 25.56 29.44] +/- [1.2  1.98 1.91]
Mean f1 score for all classes: 40.4 \pm 1.7

`Unigrams`
```
Evaluation for model: NB
Cross validaton results:
Mean precision: [88.68 34.64 34.52] +/- [1.04 2.04 2.98]
Mean recall: [66.75 56.34 65.82] +/- [1.82 3.86 2.99]
Mean f1: [76.16 42.88 45.24] +/- [1.5  2.53 3.04]
Mean f1 score for all classes: 54.8 \pm 2.4

Evaluation for model: MNB
Cross validaton results:
Mean precision: [85.91 41.89 42.94] +/- [1.13 4.41 1.87]
Mean recall: [80.74 46.51 54.15] +/- [1.36 2.88 3.91]
Mean f1: [83.24 43.97 47.86] +/- [0.78 3.26 2.4 ]
Mean f1 score for all classes: 58.4 \pm 2.1

Evaluation for model: LR
Cross validaton results:
Mean precision: [83.86 45.43 48.35] +/- [0.93 3.13 5.77]
Mean recall: [87.02 40.14 42.34] +/- [1.55 3.68 3.12]
Mean f1: [85.4  42.47 45.09] +/- [1.   2.29 4.15]
Mean f1 score for all classes: 57.7 \pm 2.5
```

`Unigrams and bigrams`
```
Evaluation for model: NB
Cross validaton results:
Mean precision: [90.69 36.87 37.76] +/- [1.13 2.41 2.34]
Mean recall: [67.23 66.32 68.58] +/- [1.36 4.42 4.46]
Mean f1: [77.21 47.38 48.67] +/- [1.13 3.02 2.82]
Mean f1 score for all classes: 57.8 \pm 2.3

Evaluation for model: MNB
Cross validaton results:
Mean precision: [87.16 45.55 44.26] +/- [0.96 3.71 3.27]
Mean recall: [80.78 50.57 59.22] +/- [1.23 3.16 2.74]
Mean f1: [83.84 47.89 50.6 ] +/- [0.96 3.22 2.71]
Mean f1 score for all classes: 60.8 \pm 2.3

Evaluation for model: LR
Cross validaton results:
Mean precision: [82.07 55.74 55.4 ] +/- [0.96 6.1  4.24]
Mean recall: [92.41 33.07 34.1 ] +/- [0.92 2.66 3.61]
Mean f1: [86.93 41.42 42.05] +/- [0.62 3.43 3.08]
Mean f1 score for all classes: 56.8 \pm 2.4

```

`Uni-bi-trigrams`
```
Evaluation for model: NB
Cross validaton results:
Mean precision: [92.73 30.36 35.18] +/- [0.9  1.18 2.66]
Mean recall: [57.34 73.68 69.68] +/- [1.1  2.64 4.44]
Mean f1: [70.85 42.97 46.65] +/- [0.88 1.19 2.56]
Mean f1 score for all classes: 53.5 \pm 1.5

Evaluation for model: MNB
Cross validaton results:
Mean precision: [89.84 38.28 39.22] +/- [0.92 2.94 1.86]
Mean recall: [71.33 61.64 65.57] +/- [1.07 2.14 2.55]
Mean f1: [79.51 47.17 49.06] +/- [0.82 2.5  1.89]
Mean f1 score for all classes: 58.6 \pm 1.7

Evaluation for model: LR
Cross validaton results:
Mean precision: [80.83 56.99 56.36] +/- [0.8  5.33 4.9 ]
Mean recall: [94.24 27.37 27.08] +/- [0.75 2.18 2.56]
Mean f1: [87.01 36.83 36.5 ] +/- [0.44 1.97 2.96]
Mean f1 score for all classes: 53.4 \pm 1.8
```

# Only neigborhood words for each entity

bigrams
Evaluation for model: NB
Cross validaton results:
Mean precision: [88.89 35.11 34.  ] +/- [0.8  2.74 3.03]
Mean recall: [65.82 57.46 67.03] +/- [1.09 2.84 2.89]
Mean f1: [75.63 43.56 45.05] +/- [0.73 2.77 3.05]
Mean f1 score for all classes: 54.7 \pm 2.2

Evaluation for model: MNB
Cross validaton results:
Mean precision: [86.02 42.24 42.19] +/- [0.89 3.54 2.99]
Mean recall: [80.77 46.88 53.55] +/- [0.69 3.23 2.59]
Mean f1: [83.31 44.35 47.15] +/- [0.45 2.81 2.5 ]
Mean f1 score for all classes: 58.3 \pm 1.9

Evaluation for model: LR
Cross validaton results:
Mean precision: [83.68 45.18 47.83] +/- [1.   3.16 3.21]
Mean recall: [86.88 39.77 42.14] +/- [0.93 3.25 2.66]
Mean f1: [85.24 42.27 44.69] +/- [0.51 3.02 1.76]
Mean f1 score for all classes: 57.4 \pm 1.8


unigram and bigrams
Evaluation for model: NB
Cross validaton results:
Mean precision: [90.32 36.   37.07] +/- [1.1  2.88 2.87]
Mean recall: [67.11 63.05 68.63] +/- [1.08 2.89 3.28]
Mean f1: [77.   45.78 48.08] +/- [0.99 2.79 2.86]
Mean f1 score for all classes: 57.0 \pm 2.2

Evaluation for model: MNB
Cross validaton results:
Mean precision: [87.54 45.04 43.54] +/- [0.65 4.07 3.3 ]
Mean recall: [79.79 51.78 60.68] +/- [1.51 3.88 3.34]
Mean f1: [83.48 48.09 50.64] +/- [0.81 3.53 2.97]
Mean f1 score for all classes: 60.7 \pm 2.4

Evaluation for model: LR
Cross validaton results:
Mean precision: [82.18 54.45 54.78] +/- [1.02 6.93 3.43]
Mean recall: [91.86 33.63 35.45] +/- [0.94 3.64 4.09]
Mean f1: [86.75 41.48 42.98] +/- [0.78 4.28 3.89]
Mean f1 score for all classes: 57.1 \pm 3.0